
#include "keymap.h"
#include "assert.h"

namespace Tests {
	struct Test_Keymap {

		static void test() {
			KeyMap keymap;

			keymap.updateKey(sf::Keyboard::A, true);
			throw_assert(keymap.keyDown(sf::Keyboard::A), "keymap test failed");
			throw_assert(keymap.keyPressed(sf::Keyboard::A), "keymap test failed");

			keymap.swapState();
			throw_assert(!keymap.keyPressed(sf::Keyboard::A), "keymap test failed");
			throw_assert(keymap.keyDown(sf::Keyboard::A), "keymap test failed");

			keymap.swapState();
			keymap.updateKey(sf::Keyboard::A, false);
			throw_assert(keymap.keyReleased(sf::Keyboard::A), "keymap test failed");
			throw_assert(!keymap.keyDown(sf::Keyboard::A), "keymap test failed");
		}
	};
}