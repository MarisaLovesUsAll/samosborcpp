#pragma once

#include <type_traits>

using TypeHash = size_t;

// Is Subset in Collection
template <typename ... TCollection>
struct TypesIntersection {

	template <typename TFind, typename T, typename ... Ts>
	static constexpr bool typeInCollection() {
		if constexpr (sizeof...(Ts) > 0)
		{
			return typeInCollection<TFind, T>() || typeInCollection<TFind, Ts...>();
		}
		else
		{
			return std::is_same<TFind, T>::value;
		}
	}

	template <typename T, typename ... TSubset>
	static constexpr bool contains() {
		if constexpr (sizeof...(TSubset) > 0)
		{
			return contains<T>() && contains<TSubset...>();
		}
		else
		{
			return typeInCollection<T, TCollection...>();
		}
	}
};

template <typename T, typename ... TComponents>
constexpr size_t computeObjectSize() {
	if constexpr (sizeof...(TComponents) > 0)
	{
		return computeObjectSize<T>() + computeObjectSize<TComponents...>();
	}
	else
	{
		return sizeof(T);
	}
}

// djb2 hash function
template <typename T, typename ... TComponents>
constexpr TypeHash computeTypeHash(size_t hash = 5381) {

	if constexpr (sizeof...(TComponents) > 0)
	{
		return computeTypeHash<TComponents...>(computeTypeHash<T>(hash));
	}
	else
	{
		return ((hash << 5) + hash) + typeid(T).hash_code();
	}
}

template <typename T, typename ... TS>
constexpr size_t ArgsSize() {
	if constexpr (sizeof...(TS) > 0)
	{
		return ArgsSize<T>() + ArgsSize<TS...>();
	}
	else
	{
		return 1;
	}
}

#include <cstddef>
#include <tuple>
#include <utility>

namespace ae {

template <typename Tuple, typename F, std::size_t ...Indices>
void for_each_impl(Tuple&& tuple, F&& f, std::index_sequence<Indices...>) {
	using swallow = int[];
	(void)swallow {
		1,
			(f(std::get<Indices>(std::forward<Tuple>(tuple))), void(), int{})...
	};
}

template <typename Tuple, typename F>
void for_each(Tuple&& tuple, F&& f) {
	constexpr std::size_t N = std::tuple_size<std::remove_reference_t<Tuple>>::value;
	for_each_impl(std::forward<Tuple>(tuple), std::forward<F>(f),
		std::make_index_sequence<N>{});
}

}