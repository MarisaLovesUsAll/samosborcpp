#include "utils.h"

#include "../assert.h"

namespace Tests {


	void template_utils_test1() {
		static_assert(TypesIntersection<int, float>::template contains<int>(), "TypesIntersection test failed");
		static_assert(TypesIntersection<int, float>::template contains<float>(), "TypesIntersection test failed");
		static_assert(!TypesIntersection<int>::template contains<float, int>(), "TypesIntersection test failed");
		static_assert(TypesIntersection<int, float>::template contains<float, int>(), "TypesIntersection test failed");
	}

	void template_utils_test2() {
		bool fuck = computeTypeHash<int>() != computeTypeHash<int, float>();
		throw_assert(fuck, "Fuck");
	}

	void for_each_test() {

		int counter = 0;
		auto tuple = std::make_tuple(1, 2, 3);

		ae::for_each(tuple, [&](auto&) {
			counter++;
		});
	}

	// all
	void template_utils() {
		template_utils_test1();
		template_utils_test2();
		for_each_test();
	}
}