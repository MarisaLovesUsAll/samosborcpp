#pragma once

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

class Camera {
public:

	// position, rotation

	glm::vec3 position;
	glm::quat rotation;

	glm::mat4 getViewMatrix() {
		glm::mat4 viewMatrix = glm::mat4(1.0f);
		viewMatrix = glm::translate(viewMatrix, position);
		viewMatrix = viewMatrix * glm::mat4_cast(rotation);
		return viewMatrix;
	}

	// projection shit

	enum CameraProjectionType {
		CameraProjectionType_Perspective,
		CameraProjectionType_Orthographic
	};

	// writeonly parameters
	CameraProjectionType projectionType;
	float perspectiveFov = 90;
	float width;
	float height;
	float nearPlane = 1; // TODO: logarithmic z-buffer
	float farPlane = 500;

	glm::mat4 projectionMatrix;

	void setProjectionPerspective(float perspectiveFov, float width, float height, float nearPlane, float farPlane) {

		this->perspectiveFov = perspectiveFov;
		this->width = width;
		this->height = height;
		this->nearPlane = nearPlane;
		this->farPlane = farPlane;

		projectionType = CameraProjectionType_Perspective;
		projectionMatrix = glm::perspectiveFov(perspectiveFov, width, height, nearPlane, farPlane);
	}

	void setProjectionOrthographic(float width, float height, float nearPlane, float farPlane) {

		this->perspectiveFov = 0;
		this->width = width;
		this->height = height;
		this->nearPlane = nearPlane;
		this->farPlane = farPlane;

		projectionType = CameraProjectionType_Orthographic;

		projectionMatrix = glm::ortho(
			-width / 2,
			width / 2,
			-height / 2,
			height / 2,
			nearPlane, farPlane);
	}

	glm::mat4 getProjectionMatrix() {
		return projectionMatrix;
	}
};
