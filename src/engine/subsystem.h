#pragma once

// Non-ECS system

#include "globalcontext.h"

class Subsystem {
public:
	virtual void init(GlobalContext& context) = 0;
	virtual void loop(GlobalContext& context) = 0;
	virtual void end(GlobalContext& context) = 0;
};
