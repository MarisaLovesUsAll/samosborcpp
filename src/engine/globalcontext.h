#pragma once

#include <memory>
#include <SFML/Window.hpp>

struct GlobalContext {

	//SubsystemGraphics* sGraphics = nullptr;
	//SubsystemWindow* sWindow = nullptr;

	bool isApplicationRunning = true;

	Camera mainCamera;
	float deltaTime;
	sf::Clock clock;
	KeyMap keyMap;
	std::unique_ptr<sf::Window> window;
	bool windowResized = true;
	int windowWidth = 0;
	int windowHeight = 0;
};
