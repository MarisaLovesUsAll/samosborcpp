#pragma once

#include <tuple>
#include <functional>
#include <list>
#include <vector>
#include <cstdint>
#include <typeindex>
#include <typeinfo>
#include <any>
#include <map>
#include <iostream>
#include <algorithm>
#include <type_traits>

#include "template/utils.h"

// Forward declarations

template <typename ... SystemsAndComponents>
class ECSManager;

template <typename ... TSystemComponents>
class SystemIterable;

using TypeHash = size_t;

// Implementation

struct Entity {
	// unsafe as fuck
	size_t index = 0; // position in array
	size_t counter = 0;
	TypeHash systemTypeHash = 0; // taken from game object configuration
};

class Component {};
class ComponentSystemBase {};

template <typename ... TSystemComponents>
class ComponentSystem : public ComponentSystemBase {
public:
	SystemIterable<TSystemComponents...> iterable;

	virtual void Update(SystemIterable<TSystemComponents...>& iterable) = 0;

	void ECS_UpdateSystem() {
		Update(iterable);
	}
};

class EntityStorageBase {
public:
	std::vector<char> memory;

	size_t objectSize = 0;
	std::map<TypeHash, size_t> componentOffsets; // TypeHash, address offset

	template <typename T>
	T* GetComponent(Entity entity) {
		char* address = &memory[entity.index * objectSize];
		size_t offset = componentOffsets[computeTypeHash<T>()];

		return reinterpret_cast<T*>(address + offset);
	}

private:
	//template <typename T>
	//size_t GetComponentOffset() {
	//	auto it = componentOffsets.find(computeTypeHash<T>());
	//	if (it == componentOffsets.end())
	//	{
	//		throw "No component " + typeid(T) + " found in EntityStorage";
	//	}
	//	return *it;
	//}
};

template <typename ... TComponents>
class EntityStorage : public EntityStorageBase {

	template<size_t offset = 0x0, typename TCurrent, typename ... TRemaining>
	void InitComponentOffsets() {
		componentOffsets[computeTypeHash<TCurrent>()] = offset;
		if constexpr (sizeof...(TRemaining) > 0) {
			InitComponentOffsets<offset + sizeof(TCurrent), TRemaining...>();
		}
	}

public:

	EntityStorage() {
		objectSize = computeObjectSize<TComponents...>();
		InitComponentOffsets<0, TComponents...>();
	}

	//static inline const size_t objectSize = computeObjectSize<TComponents...>();

	size_t getSize() {
		return memory.size() / objectSize;
	}

	size_t maxCounter = 1;

	Entity CreateEntity() {

		Entity entity;

		const size_t memorySizeBefore = memory.size();

		try {
			entity.index = getSize();
			entity.counter = maxCounter++;
			entity.systemTypeHash = computeTypeHash<TComponents...>();

			memory.resize(memory.size() + objectSize);

			char* address = memory.data() + memorySizeBefore;

			CreateComponents<0, TComponents...>(address);
		}
		catch (...)
		{
			memory.resize(memorySizeBefore);
		}
		return entity;
	}


	template<size_t offset = 0x0, typename TCurrent, typename ... TRemaining>
	void CreateComponents(char* address) {

		new(address + offset) TCurrent();

		if constexpr (sizeof...(TRemaining) > 0) {
			CreateComponents<offset + sizeof(TCurrent), TRemaining...>(address);
		}
	}

	void DeleteEntity(Entity& entity) {
		char* address = memory.data() + entity.index * objectSize;
		DeleteComponents<0, TComponents...>(address);

		entity.index = 0;
	}

	template<size_t offset = 0x0, typename TCurrent, typename ... TRemaining>
	void DeleteComponents(char* address) {

		reinterpret_cast<TCurrent>(address).~TCurrent();

		if constexpr (sizeof...(TRemaining) > 0) {
			DeleteComponents<offset + sizeof(TCurrent), TRemaining...>(address);
		}
	}
};


// 100% constexpr
template <size_t sourceAddressOffset = 0, typename TFind, typename T, typename ... TStorageComponents_>
constexpr size_t getComponentAddressOffset() {

	if constexpr (std::is_same<TFind, T>::value)
	{
		return sourceAddressOffset;
	}
	else if constexpr (sizeof...(TStorageComponents_) > 0)
	{
		return getComponentAddressOffset<sourceAddressOffset + sizeof(T), TFind, TStorageComponents_...>();
	}
	else
	{
		return -1;
	}
}

template <typename ... SystemsAndComponents>
class ECSManager {
public:

	ECSManager()
	{
		systems = CreateSystems<SystemsAndComponents...>();
	}

	template <typename T, typename ... TS>
	constexpr auto CreateSystems() 
	{
		if constexpr (sizeof...(TS) > 0)
		{
			return std::tuple_cat(CreateSystems<T>(), CreateSystems<TS...>());
		}
		else
		{
			if constexpr (std::is_convertible<T, ComponentSystemBase>::value)
			{
				return std::make_tuple(std::make_unique<T>());
			}
			else
			{
				return std::make_tuple();
			}
		}
	}

	typedef ECSManager<SystemsAndComponents...> RootType;
	using SystemsTuple = typename std::result_of<decltype(&RootType::CreateSystems<SystemsAndComponents...>)(RootType)>::type;
	SystemsTuple systems;

	time_t previousFrameTime = 0;
	const time_t DESIRABLE_FPS = 5;

	void UpdateAllSystems() {

		time_t frameTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		time_t delta = frameTime - previousFrameTime;
		time_t sleepTime = std::clamp<time_t>(1000 / DESIRABLE_FPS - delta, 0, 1000 / DESIRABLE_FPS);
		previousFrameTime = frameTime;
		sf::sleep(sf::milliseconds((sf::Int32)sleepTime));

		ae::for_each(systems, [](auto& system) {
			system->ECS_UpdateSystem();
		});
	}

	std::map<size_t, EntityStorageBase*> entityStorageMap;

	template<typename ... T>
	Entity CreateGameObject() {

		const size_t hash = computeTypeHash<T...>();

		EntityStorage<T...>* storage = nullptr;

		const auto it = entityStorageMap.find(hash);

		if (it == entityStorageMap.end())
		{
			storage = new EntityStorage<T...>();
			entityStorageMap[hash] = reinterpret_cast<EntityStorageBase*>(storage);

			AddStorageToSystems(storage);
		}
		else
		{
			storage = reinterpret_cast<EntityStorage<T...>*>(it->second);
		}

		Entity entity = storage->CreateEntity();
		return entity;
	}

	// runtime
	template <typename TComponent>
	TComponent* GetComponent(Entity entity)
	{
		EntityStorageBase* system = entityStorageMap[entity.systemTypeHash];
		return system->GetComponent<TComponent>(entity);
	}

	template <typename ... TStorageComponents >
	void AddStorageToSystems(EntityStorage<TStorageComponents...>* storage)
	{
		ae::for_each(systems, [&](auto& system) {
			system->iterable.TryAddStorage(storage);
		});
	}

	~ECSManager() {
		for (auto pair : entityStorageMap) {
			delete pair.second;
		}
	}
};


template <typename ... TSystemComponents>
class SystemIterable {
public:
	class StorageIterable;

	class StorageIterator {
	public:
		size_t position = 0;
		StorageIterable* iterable = nullptr;

		StorageIterator(StorageIterable* iterable) : iterable(iterable), position(0) {}
		StorageIterator(const StorageIterator& other) : iterable(other.iterable), position(other.position) {}

		template <size_t index = 0, typename T, typename ... TS >
		constexpr auto mapToIterator(char* sourceAddress) const {

			T* component = reinterpret_cast<T*>(sourceAddress + iterable->offsets[index]);
			if constexpr (sizeof...(TS) > 0)
			{
				return std::tuple_cat(std::make_tuple(component), mapToIterator<index + 1, TS...>(sourceAddress));
			}
			else
			{
				return std::make_tuple(component);
			}
		}

		inline StorageIterator& operator++() {
			position++;
			return *this;
		}

		inline StorageIterator& operator++(int) {
			position++;
			return *this;
		}

		// @returns std::tuple<TSystemComponents*...>
		inline auto operator*() const {

#if _DEBUG
			// Bounds check
			if (position < 0 || position >= iterable->storage->memory.size() / iterable->objectSize)
			{
				throw std::exception("Out of bounds exception");
			}
#endif

			auto tuple = mapToIterator<0, TSystemComponents...>(&(iterable->storage->memory[position * iterable->objectSize]));
			return tuple;
		}


		//template <size_t tuplePosition = 0, typename T, typename ... TSystemComponents_>
		//constexpr auto mapToIterator(char* sourceAddress) {

		//	size_t currentTOffset = getComponentAddressOffset<0, T, TStorageComponents>();

		//	static_assert(currentTOffset == -1, "currentTOffset == -1");

		//	T* component = reinterpret_cast<T*>(sourceAddress + currentTOffset);

		//	if constexpr (sizeof...(TSystemComponents_) > 0) 
		//	{
		//		return std::tuple_cat(hana::make_tuple(component), mapToIterator<TSystemComponents_...>(sourceAddress));
		//	}
		//	else 
		//	{
		//		return std::make_tuple(component);
		//	}
		//}


		//typedef SystemIterable<TSystemsComponents...> RootType;
		//typedef typename std::result_of<decltype(&RootType::mapToIterator<TSystemsComponents...>)(RootType)>::type T_SystemComponentsPointers;


		inline bool operator!=(const StorageIterator& other) const {
			return !(*this == other);
		}

		inline bool operator==(const StorageIterator& other) const {
			return position == other.position
#if _DEBUG
				&& iterable == other.iterable
#endif
				;
		}
	};

	class StorageIterable {
	public:

		EntityStorageBase* storage;
		size_t objectSize = 0;

		StorageIterable(EntityStorageBase* storage, size_t objectSize) : storage(storage), objectSize(objectSize) {

		}

		size_t offsets[sizeof...(TSystemComponents)];

		template <typename ... TStorageComponents>
		struct Helper {

			template<size_t index = 0, typename T, typename ... TSystemComponents_>
			static constexpr void initAddressOffsetImpl(size_t* offsets_) {

				offsets_[index] = getComponentAddressOffset<0, T, TStorageComponents...>();

				if constexpr (sizeof...(TSystemComponents_) > 0)
				{
					Helper<TStorageComponents...>::template initAddressOffsetImpl<index + 1, TSystemComponents_...>(offsets_);
				}
			}
		};

		// Init offsets map with storage type.
		template <typename ... TStorageComponents >
		constexpr inline void initAddressOffset() {

			Helper<TStorageComponents...>::template initAddressOffsetImpl<0, TSystemComponents...>(offsets);
		}

		StorageIterator begin() {
			auto it = StorageIterator(this);
			return it;
		}

		StorageIterator end() {
			StorageIterator it = StorageIterator(this);
			it.position = storage->memory.size() / objectSize;
			return it;
		}
	};


	// this function is just for the types
	template <typename T, typename ... TS>
	constexpr auto SystemPointersFunction() {
		if constexpr (sizeof...(TS) > 0)
		{
			return std::tuple_cat(std::tuple<T*>(), SystemPointersFunction<TS...>());
		}
		else
		{
			return std::tuple<T*>();
		}
	}

	typedef SystemIterable<TSystemComponents...> RootType;
	using SystemPointers = typename std::result_of<decltype(&RootType::SystemPointersFunction<TSystemComponents...>)(RootType)>::type;
	
	void fuck() {}

	template <typename ... TStorageComponents>
	void TryAddStorage(EntityStorage<TStorageComponents...>* storage) {

		if constexpr (TypesIntersection<TStorageComponents...>::template contains<TSystemComponents...>())
		{
			StorageIterable fuck(storage, storage->objectSize);
			fuck.template initAddressOffset<TStorageComponents...>();
			storageIterables.push_back(std::move(fuck));
		}
	}

	std::vector<StorageIterable> storageIterables;


	class SystemIterator {
	public:

		std::vector<std::pair<StorageIterator, StorageIterator>> iterators;

		size_t position = 0;

		SystemIterator(std::vector<StorageIterable>& list) {
			for (auto& iterable : list)
			{
				iterators.push_back(std::make_pair(iterable.begin(), iterable.end()));
			}
		}

		SystemIterator& operator++() {
			auto& [storageCurrent, storageEnd] = iterators[position];

			storageCurrent++;

			if (storageCurrent == storageEnd)
			{
				position++;
				if (position != iterators.size())
				{
					operator++();
				}
			}

			return (*this);
		}

		SystemPointers operator*() const {
			auto& [storageCurrent, storageEnd] = iterators[position];
			return (*storageCurrent);
		}

		bool operator!=(const SystemIterator& other) {
			return !((*this) == other);
		}

		bool operator==(const SystemIterator& other) {
			return position == other.position 
#if _DEBUG
				//&& iterators[position].first == iterators[other.position].first
#endif				
				;
		}
	};

	SystemIterator begin() {
		return SystemIterator(storageIterables);
	}

	SystemIterator end() {
		SystemIterator it(storageIterables);
		it.position = it.iterators.size();
		return it;
	}

/*
	Iterator begin() {
		return 
	}
	Iterator end() {
	}*/
};

struct TestComponent : public Component {
	int field = 1337;
};

struct TestComponent2 : public Component {
	int field = 1488;
};

class TestSystem : public ComponentSystem<TestComponent, TestComponent2> {
	void Update(SystemIterable<TestComponent, TestComponent2>& state) {
	//	std::cout << "TestSystem update: ";
	//	for (hana::tuple<TestComponent*, TestComponent2*>& obj : state) {

	//		auto testComponent = hana::at(obj, hana::int_c<0>);
	//		auto testComponent2 = hana::at(obj, hana::int_c<1>);

	//		//auto& [testComponent] = obj;

	//		std::cout << obj[0_c]->field;
	//		//std::cout << testComponent->field << ' ' << testComponent2->field << '\n';
	//	}
	//	std::cout << '\n';
	}
};
