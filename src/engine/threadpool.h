#pragma once

#include <string>
#include <list>
#include <functional>
#include <thread>
#include <condition_variable>
#include <atomic>
#include <mutex>

const size_t THREAD_COUNT = 10;

class ThreadPool {
	std::list<std::thread> threads;

	std::list<std::function<void(void)>> queue;
	std::mutex queue_mutex;

	std::condition_variable cv;
	std::mutex waitMutex;

	std::atomic_bool isRunning = true;

	void queueTask(std::function<void(void)> func) {
		queue.push_back(func);
	}

	ThreadPool()
	{
		for (size_t i = 0; i < THREAD_COUNT; i++)
		{
			std::thread thread(threadRoutine);
			thread.detach();
			threads.push_back(thread);
		}
	}

	~ThreadPool()
	{
		isRunning.store(false);
		cv.notify_all();
		for (auto& thread : threads) {
			cv.notify_one();
			thread->join();
		}
	}

	void threadRoutine() {
		while (isRunning.load()) {
			std::function<void(void)> task* = nullptr;

			{
				std::scoped_lock lock(queue_mutex);
				auto& it = queue.begin();
				if (it != queue.end())
				{
					task = *it;
					queue.pop_front();
				}
			}
			if (task) {
				(*task)();
			}
			cv.wait(mutex);
		}
	}
};
