#pragma once

#include "template/utils.test.h"
#include "keymap.test.h"
// TODO: add some test framework here

namespace Tests {
	void run() {
		template_utils();
		Test_Keymap::test();

		std::cout << "Tests completed\n";
	}
}