#pragma once

class SystemLogicOld : public System {
public:
	CameraObject cameraObject;

	void init(GlobalContext& context)
	{
		context.mainCamera.position = glm::vec3(0, 0, -4);
	}

	void loop(GlobalContext& context) {

		// Request a 24-bits depth buffer when creating the window
		sf::ContextSettings contextSettings;
		contextSettings.depthBits = 24;
		contextSettings.sRgbCapable = true;

		// Create the main window
		sf::RenderWindow window(sf::VideoMode(800, 600), "SFML graphics with OpenGL", sf::Style::Default, contextSettings);
		window.setVerticalSyncEnabled(true);


		// Load a texture to apply to our 3D cube
		sf::Texture texture;
		if (!texture.loadFromFile(resourcesDir() + "texture.jpg")) {
			throw std::exception("texture not found");
		}

		// Attempt to generate a mipmap for our cube texture
		// We don't check the return value here since
		// mipmapping is purely optional in this example
		texture.generateMipmap();

		// Make the window the active window for OpenGL calls
		window.setActive(true);

		auto windowHandle = window.getSystemHandle();

		// Enable Z-buffer read and write
		//glDisable(GL_DEPTH_TEST);
		glDisable(GL_CULL_FACE);
		//glEnable(GL_TEXTURE_2D);
		glEnable(GL_DEPTH_TEST);
		//glDepthMask(GL_TRUE);
		//glClearDepth(1.f);

		// Configure the viewport (the same size as the window)
		glViewport(0, 0, window.getSize().x, window.getSize().y);

		context.mainCamera.setProjectionPerspective(90.f, (float)window.getSize().x, (float)window.getSize().y, 1.0f, 500.0f);

		//context.mainCamera.setProjectionOrthographic(
		//	(float)window.getSize().x,
		//	(float)window.getSize().y,
		//	1.f, 
		//	500.f);


		// Bind the texture
		//glEnable(GL_TEXTURE_2D);
		sf::Texture::bind(&texture);


		auto glewInitCode = glewInit();

		GLubyte cubeIndices[24] = { 0,1,2,3, 4,5,6,7, 3,2,5,4, 7,6,1,0,
										 8,9,10,11, 12,13,14,15 };

		GLuint bufferIndex;
		glGenBuffers(1, &bufferIndex);
		glBindBuffer(GL_ARRAY_BUFFER, bufferIndex);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cube), cube, GL_STATIC_DRAW);

		GLuint bufferUVIndex;
		glGenBuffers(1, &bufferUVIndex);
		glBindBuffer(GL_ARRAY_BUFFER, bufferUVIndex);
		glBufferData(GL_ARRAY_BUFFER, sizeof(cube_uv), cube_uv, GL_STATIC_DRAW);


		// Enable position and texture coordinates vertex components
		//glEnableClientState(GL_VERTEX_ARRAY);
		//glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		//glVertexPointer(3, GL_FLOAT, 5 * sizeof(GLfloat), cube);
		//glTexCoordPointer(2, GL_FLOAT, 5 * sizeof(GLfloat), cube + 3);

		// Disable normal and color vertex components
		//glDisableClientState(GL_NORMAL_ARRAY);
		//glDisableClientState(GL_COLOR_ARRAY);

		// Make the window no longer the active window for OpenGL calls
		window.setActive(false);

		// Create a clock for measuring the time elapsed
		sf::Clock clock;
		sf::Clock clockDt;

		// Flag to track whether mipmapping is currently enabled
		bool mipmapEnabled = true;

		if (!shader.loadFromMemory(vertex_shader_source, frag_shader_source))
		{
			std::cout << "Da pohuy";
			// error...
		}

		// Start game loop
		while (window.isOpen())
		{
			float dt = clockDt.restart().asSeconds();



			//std::cout << cameraPosition.x << " " << cameraPosition.y << " " << cameraPosition.z << '\n';

			// Draw the background
			//window.pushGLStates();

			//window.draw(background);

			//window.popGLStates();

			// Make the window the active window for OpenGL calls
			window.setActive(true);

			// Clear the depth buffer
			glClearColor(1, 1.f, 1.f, 1.f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			//glClear(GL_DEPTH_BUFFER_BIT);

			// We get the position of the mouse cursor (or touch), so that we can move the box accordingly
			//sf::Vector2i pos;

			//pos = sf::Mouse::getPosition();

			//float x = pos.x * 200.f / window.getSize().x - 100.f;
			//float y = -pos.y * 200.f / window.getSize().y + 100.f;

			glm::mat4x4 modelMatrix = glm::mat4(1.0f);

			//modelMatrix = glm::scale(modelMatrix, glm::vec3(.01f, .01f, .01f));
			//modelMatrix = glm::rotate(modelMatrix, clock.getElapsedTime().asSeconds() * 5.f, glm::vec3(1.f, 0.f, 0.f));
			//modelMatrix = glm::rotate(modelMatrix, clock.getElapsedTime().asSeconds() * 3.f, glm::vec3(0.f, 1.f, 0.f));
			//modelMatrix = glm::rotate(modelMatrix, clock.getElapsedTime().asSeconds() * 9.f, glm::vec3(0.f, 0.f, 1.f));

			//sf::Shader::bind(&shader);

			// view matrix

			context.mainCamera.rotation = glm::quat(clock.getElapsedTime().asSeconds() * 5.f * glm::vec3(1.f, 0.f, 0.f));
			//viewMatrix = glm::rotate(viewMatrix, clock.getElapsedTime().asSeconds() * 5.f * glm::vec3(1.f, 0.f, 0.f));
			//viewMatrix = glm::transpose(viewMatrix);

			//glm::mat4 cameraRotationMat = glm::mat4_cast(cameraRotation);

			//viewMatrix = viewMatrix * cameraRotationMat;

			glm::mat4x4 mvpMatrix = context.mainCamera.getProjectionMatrix() * context.mainCamera.getViewMatrix() * modelMatrix;

			sf::Shader::bind(&shader);

			FuckOpengl();

			// Bind mvp matrix
			shader.setUniform("mvp_matrix", *reinterpret_cast<sf::Glsl::Mat4*>(&(mvpMatrix[0][0])));
			//{
			//	GLint loc = glGetUniformLocation(shader.getNativeHandle(), "mvp_matrix");
			//	FuckOpengl();
			//	if (loc != -1)
			//	{
			//		glUniformMatrix4fv(loc, 1, GL_FALSE, &(mvpMatrix[0][0]));
			//	}
			//}

			FuckOpengl();

			// Draw the cube

			glActiveTexture(GL_TEXTURE0);
			sf::Texture::bind(&texture);
			//glBindTexture(GL_TEXTURE_2D, texture.getNativeHandle());

			shader.setUniform("textureSampler", texture);

			//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, NULL);
			// Bind array buffer as vertices

			//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferIndex);

			glEnableVertexAttribArray(0);
			glBindBuffer(GL_ARRAY_BUFFER, bufferIndex);
			glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

			glEnableVertexAttribArray(1);
			glBindBuffer(GL_ARRAY_BUFFER, bufferUVIndex);
			glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, 0);

			//glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0);

			FuckOpengl();

			glDrawArrays(GL_TRIANGLES, 0, 36);

			FuckOpengl();

			// Make the window no longer the active window for OpenGL calls
			window.setActive(false);

			// Draw some text on top of our OpenGL object
			//window.pushGLStates();
			//window.draw(text);
			//window.draw(sRgbInstructions);
			//window.draw(mipmapInstructions);
			//window.popGLStates();

			// Finally, display the rendered frame on screen
			window.display();
		}

	}
	void end(GlobalContext& context) {}
};