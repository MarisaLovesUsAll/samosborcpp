#pragma once


#include "../componentsystem.h"
#include "../subsystem.h"

#include <vulkan/vulkan.hpp>

#include <vulkan/vulkan_win32.h>
#include <WinUser.h>


#define ENABLE_DEBUGGING 1
const char* DEBUG_LAYER = "VK_LAYER_LUNARG_standard_validation";

/*

gpu megatexture contains N latest used sprites
sprites are deleted after N draw calls




component Transform
component Sprite






gpu megatexture




ingame: 3d array 
2d slice of map
generate 


2d sprite
width, height
origin

*/

//
//
//class Mesh : public Component {
//
//};
//
//class Material {
//
//};
//
//class Renderer : public Component {
//	sf::Texture texture;
//
//};


const std::vector<const char*> validationLayers = {
	"VK_LAYER_KHRONOS_validation"
};

const std::vector<const char*> neededExtensions = {
	"VK_KHR_surface",
	"VK_KHR_win32_surface",

#ifdef ENABLE_DEBUGGING
	VK_EXT_DEBUG_REPORT_EXTENSION_NAME,
#endif
		
};

// Debug callback
VkBool32 debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t srcObject, size_t location, int32_t msgCode, const char* pLayerPrefix, const char* pMsg, void* pUserData) {
	if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT) {
		std::cerr << "ERROR: [" << pLayerPrefix << "] Code " << msgCode << " : " << pMsg << std::endl;
	}
	else if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT) {
		std::cerr << "WARNING: [" << pLayerPrefix << "] Code " << msgCode << " : " << pMsg << std::endl;
	}

	//exit(1);

	return VK_FALSE;
}


#ifdef NDEBUG
const bool enableValidationLayers = false;
#else
const bool enableValidationLayers = true;
#endif

struct VKContext {
	bool isInit = false;
	VkInstance instance;
	VkPhysicalDevice physicalDevice;
	VkDevice device;
	VkSurfaceKHR windowSurface;
	VkDebugReportCallbackEXT debugCallback;

	uint32_t graphicsQueueFamily;
	uint32_t presentQueueFamily;

	VkQueue graphicsQueue;
	VkQueue presentQueue;
	VkPhysicalDeviceMemoryProperties deviceMemoryProperties;

	VkSemaphore imageAvailableSemaphore;
	VkSemaphore renderingFinishedSemaphore;

	VkExtent2D swapChainExtent;
	VkFormat swapChainFormat;
	VkSwapchainKHR oldSwapChain;
	VkSwapchainKHR swapChain;

	VkRenderPass renderPass;
	VkPipeline graphicsPipeline;
	VkPipelineLayout pipelineLayout;

	VkCommandPool commandPool;
	std::vector<VkCommandBuffer> graphicsCommandBuffers;

	std::vector<VkImage> swapChainImages;
	std::vector<VkImageView> swapChainImageViews;
	std::vector<VkFramebuffer> swapChainFramebuffers;

};

class SubsystemGraphics : public Subsystem {
	VKContext vkContext;
public:
	void init(GlobalContext& context) {
		if (vkContext.isInit) {
			end(context);
		}

		vulkanInfo();

		createInstance();
		createDebugCallback();
		createWindowSurface(context);
		findPhysicalDevice();
		checkSwapChainSupport();
		findQueueFamilies();
		createLogicalDevice();
		createSemaphores();
		createCommandPool();
	}

	void loop(GlobalContext& context) {
	}

	void end(GlobalContext& context) {

		vkFreeCommandBuffers(vkContext.device, vkContext.commandPool, (uint32_t)vkContext.graphicsCommandBuffers.size(), vkContext.graphicsCommandBuffers.data());

		if (true) {
			vkDestroyCommandPool(vkContext.device, vkContext.commandPool, nullptr);

			vkDeviceWaitIdle(vkContext.device);

			vkDestroySemaphore(vkContext.device, vkContext.imageAvailableSemaphore, nullptr);
			vkDestroySemaphore(vkContext.device, vkContext.renderingFinishedSemaphore, nullptr);

			// Note: implicitly destroys images (in fact, we're not allowed to do that explicitly)
			vkDestroySwapchainKHR(vkContext.device, vkContext.swapChain, nullptr);

			vkDestroyDevice(vkContext.device, nullptr);
			vkDestroySurfaceKHR(vkContext.instance, vkContext.windowSurface, nullptr);

			if (ENABLE_DEBUGGING) {
				PFN_vkDestroyDebugReportCallbackEXT DestroyDebugReportCallback = (PFN_vkDestroyDebugReportCallbackEXT)vkGetInstanceProcAddr(vkContext.instance, "vkDestroyDebugReportCallbackEXT");
				DestroyDebugReportCallback(vkContext.instance, vkContext.debugCallback, nullptr);
			}

			vkDestroyInstance(vkContext.instance, nullptr);
		}
	}

private:

	//sf::Window window;
	void vulkanInfo() {

		uint32_t extensionCount = 0;
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

		std::cout << "Vulkan info:" << std::endl;
		std::cout << extensionCount << " extensions supported\n" << std::endl;
	}
	void createInstance() {
		VkApplicationInfo appInfo = {};
		appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
		appInfo.pApplicationName = "VulkanClear";
		appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.pEngineName = "ClearScreenEngine";
		appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
		appInfo.apiVersion = VK_API_VERSION_1_0;

		// Check for extensions
		uint32_t extensionCount = 0;
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

		if (extensionCount == 0) {
			std::cerr << "no extensions supported!" << std::endl;
			exit(1);
		}

		std::vector<VkExtensionProperties> availableExtensions(extensionCount);
		vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, availableExtensions.data());

		std::cout << "supported extensions:" << std::endl;

		for (const auto& extension : availableExtensions) {
			std::cout << "\t" << extension.extensionName << std::endl;
		}

		VkInstanceCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
		createInfo.pApplicationInfo = &appInfo;
		createInfo.enabledExtensionCount = (uint32_t)neededExtensions.size();
		createInfo.ppEnabledExtensionNames = neededExtensions.data();

		if (ENABLE_DEBUGGING) {
			createInfo.enabledLayerCount = 1;
			createInfo.ppEnabledLayerNames = &DEBUG_LAYER;
		}

		// Initialize Vulkan instance
		auto vkCreateInstance_code = vkCreateInstance(&createInfo, nullptr, &vkContext.instance);
		if (vkCreateInstance_code != VK_SUCCESS) {
			std::cerr << "failed to create instance! Code: " << vkCreateInstance_code << std::endl;
			exit(1);
		}
		else {
			std::cout << "created vulkan instance" << std::endl;
		}
	}

	void createDebugCallback() {
		if constexpr(ENABLE_DEBUGGING) {
			VkDebugReportCallbackCreateInfoEXT createInfo = {};
			createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
			createInfo.pfnCallback = (PFN_vkDebugReportCallbackEXT)debugCallback;
			createInfo.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT | VK_DEBUG_REPORT_WARNING_BIT_EXT;

			PFN_vkCreateDebugReportCallbackEXT CreateDebugReportCallback = (PFN_vkCreateDebugReportCallbackEXT)vkGetInstanceProcAddr(vkContext.instance, "vkCreateDebugReportCallbackEXT");

			if (CreateDebugReportCallback(vkContext.instance, &createInfo, nullptr, &vkContext.debugCallback) != VK_SUCCESS) {
				std::cerr << "failed to create debug callback" << std::endl;
				exit(1);
			}
			else {
				std::cout << "created debug callback" << std::endl;
			}
		}
		else {
			std::cout << "skipped creating debug callback" << std::endl;
		}
	}

	void createWindowSurface(GlobalContext& context)
	{
		VkWin32SurfaceCreateInfoKHR createInfo = {};

		sf::WindowHandle hwnd = context.window->getSystemHandle();
		LONG hinstance = GetWindowLong(hwnd, GWLP_HINSTANCE);
		
		createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
		createInfo.hwnd = hwnd;
		createInfo.hinstance = (HINSTANCE) hinstance;

		vkCreateWin32SurfaceKHR(vkContext.instance, &createInfo, NULL, &vkContext.windowSurface);
	}
	
	void findPhysicalDevice() {

		// Try to find 1 Vulkan supported device
		// Note: perhaps refactor to loop through devices and find first one that supports all required features and extensions

		uint32_t deviceCount = 0;
		if (vkEnumeratePhysicalDevices(vkContext.instance, &deviceCount, nullptr) != VK_SUCCESS || deviceCount == 0) {
			std::cerr << "failed to get number of physical devices" << std::endl;
			exit(1);
		}

		deviceCount = 1;
		VkResult res = vkEnumeratePhysicalDevices(vkContext.instance, &deviceCount, &vkContext.physicalDevice);
		if (res != VK_SUCCESS && res != VK_INCOMPLETE) {
			std::cerr << "enumerating physical devices failed!" << std::endl;
			exit(1);
		}

		if (deviceCount == 0) {
			std::cerr << "no physical devices that support vulkan!" << std::endl;
			exit(1);
		}

		std::cout << "physical device with vulkan support found" << std::endl;

		// Check device features
		// Note: will apiVersion >= appInfo.apiVersion? Probably yes, but spec is unclear.
		VkPhysicalDeviceProperties deviceProperties;
		VkPhysicalDeviceFeatures deviceFeatures;
		vkGetPhysicalDeviceProperties(vkContext.physicalDevice, &deviceProperties);
		vkGetPhysicalDeviceFeatures(vkContext.physicalDevice, &deviceFeatures);

		uint32_t supportedVersion[] = {
			VK_VERSION_MAJOR(deviceProperties.apiVersion),
			VK_VERSION_MINOR(deviceProperties.apiVersion),
			VK_VERSION_PATCH(deviceProperties.apiVersion)
		};

		std::cout << "physical device supports version " << supportedVersion[0] << "." << supportedVersion[1] << "." << supportedVersion[2] << std::endl;

	}

	void checkSwapChainSupport() {

		uint32_t extensionCount = 0;
		vkEnumerateDeviceExtensionProperties(vkContext.physicalDevice, nullptr, &extensionCount, nullptr);

		if (extensionCount == 0) {
			std::cerr << "physical device doesn't support any extensions" << std::endl;
			exit(1);
		}

		std::vector<VkExtensionProperties> deviceExtensions(extensionCount);
		vkEnumerateDeviceExtensionProperties(vkContext.physicalDevice, nullptr, &extensionCount, deviceExtensions.data());

		for (const auto& extension : deviceExtensions) {
			if (strcmp(extension.extensionName, VK_KHR_SWAPCHAIN_EXTENSION_NAME) == 0) {
				std::cout << "physical device supports swap chains" << std::endl;
				return;
			}
		}

		std::cerr << "physical device doesn't support swap chains" << std::endl;
		exit(1);
	}

	void findQueueFamilies() {
		// Check queue families
		uint32_t queueFamilyCount = 0;
		vkGetPhysicalDeviceQueueFamilyProperties(vkContext.physicalDevice, &queueFamilyCount, nullptr);

		if (queueFamilyCount == 0) {
			std::cout << "physical device has no queue families!" << std::endl;
			exit(1);
		}

		// Find queue family with graphics support
		// Note: is a transfer queue necessary to copy vertices to the gpu or can a graphics queue handle that?
		std::vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);
		vkGetPhysicalDeviceQueueFamilyProperties(vkContext.physicalDevice, &queueFamilyCount, queueFamilies.data());

		std::cout << "physical device has " << queueFamilyCount << " queue families" << std::endl;

		bool foundGraphicsQueueFamily = false;
		bool foundPresentQueueFamily = false;

		for (uint32_t i = 0; i < queueFamilyCount; i++) {
			VkBool32 presentSupport = false;
			vkGetPhysicalDeviceSurfaceSupportKHR(vkContext.physicalDevice, i, vkContext.windowSurface, &presentSupport);

			if (queueFamilies[i].queueCount > 0 && queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
				vkContext.graphicsQueueFamily = i;
				foundGraphicsQueueFamily = true;

				if (presentSupport) {
					vkContext.presentQueueFamily = i;
					foundPresentQueueFamily = true;
					break;
				}
			}

			if (!foundPresentQueueFamily && presentSupport) {
				vkContext.presentQueueFamily = i;
				foundPresentQueueFamily = true;
			}
		}

		if (foundGraphicsQueueFamily) {
			std::cout << "queue family #" << vkContext.graphicsQueueFamily << " supports graphics" << std::endl;

			if (foundPresentQueueFamily) {
				std::cout << "queue family #" << vkContext.presentQueueFamily << " supports presentation" << std::endl;
			}
			else {
				std::cerr << "could not find a valid queue family with present support" << std::endl;
				exit(1);
			}
		}
		else {
			std::cerr << "could not find a valid queue family with graphics support" << std::endl;
			exit(1);
		}
	}

	void createLogicalDevice() {
		// Greate one graphics queue and optionally a separate presentation queue
		float queuePriority = 1.0f;

		VkDeviceQueueCreateInfo queueCreateInfo[2] = {};

		queueCreateInfo[0].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo[0].queueFamilyIndex = vkContext.graphicsQueueFamily;
		queueCreateInfo[0].queueCount = 1;
		queueCreateInfo[0].pQueuePriorities = &queuePriority;

		queueCreateInfo[1].sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		queueCreateInfo[1].queueFamilyIndex = vkContext.presentQueueFamily;
		queueCreateInfo[1].queueCount = 1;
		queueCreateInfo[1].pQueuePriorities = &queuePriority;

		// Create logical device from physical device
		// Note: there are separate instance and device extensions!
		VkDeviceCreateInfo deviceCreateInfo = {};
		deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
		deviceCreateInfo.pQueueCreateInfos = queueCreateInfo;

		if (vkContext.graphicsQueueFamily == vkContext.presentQueueFamily) {
			deviceCreateInfo.queueCreateInfoCount = 1;
		}
		else {
			deviceCreateInfo.queueCreateInfoCount = 2;
		}

		// Necessary for shader (for some reason)
		VkPhysicalDeviceFeatures enabledFeatures = {};
		enabledFeatures.shaderClipDistance = VK_TRUE;
		enabledFeatures.shaderCullDistance = VK_TRUE;

		const char* deviceExtensions = VK_KHR_SWAPCHAIN_EXTENSION_NAME;
		deviceCreateInfo.enabledExtensionCount = 1;
		deviceCreateInfo.ppEnabledExtensionNames = &deviceExtensions;
		deviceCreateInfo.pEnabledFeatures = &enabledFeatures;

		if (ENABLE_DEBUGGING) {
			deviceCreateInfo.enabledLayerCount = 1;
			deviceCreateInfo.ppEnabledLayerNames = &DEBUG_LAYER;
		}

		if (vkCreateDevice(vkContext.physicalDevice, &deviceCreateInfo, nullptr, &vkContext.device) != VK_SUCCESS) {
			std::cerr << "failed to create logical device" << std::endl;
			exit(1);
		}

		std::cout << "created logical device" << std::endl;

		// Get graphics and presentation queues (which may be the same)
		vkGetDeviceQueue(vkContext.device, vkContext.graphicsQueueFamily, 0, &vkContext.graphicsQueue);
		vkGetDeviceQueue(vkContext.device, vkContext.presentQueueFamily, 0, &vkContext.presentQueue);

		std::cout << "acquired graphics and presentation queues" << std::endl;

		vkGetPhysicalDeviceMemoryProperties(vkContext.physicalDevice, &vkContext.deviceMemoryProperties);
	}

	void createSemaphores() {
		VkSemaphoreCreateInfo createInfo = {};
		createInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

		if (vkCreateSemaphore(vkContext.device, &createInfo, nullptr, &vkContext.imageAvailableSemaphore) != VK_SUCCESS ||
			vkCreateSemaphore(vkContext.device, &createInfo, nullptr, &vkContext.renderingFinishedSemaphore) != VK_SUCCESS) {
			std::cerr << "failed to create semaphores" << std::endl;
			exit(1);
		}
		else {
			std::cout << "created semaphores" << std::endl;
		}
	}

	void createCommandPool() {
		// Create graphics command pool
		VkCommandPoolCreateInfo poolCreateInfo = {};
		poolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
		poolCreateInfo.queueFamilyIndex = vkContext.graphicsQueueFamily;

		if (vkCreateCommandPool(vkContext.device, &poolCreateInfo, nullptr, &vkContext.commandPool) != VK_SUCCESS) {
			std::cerr << "failed to create command queue for graphics queue family" << std::endl;
			exit(1);
		}
		else {
			std::cout << "created command pool for graphics queue family" << std::endl;
		}
	}

	//void createVertexBuffer() {
	//	// Setup vertices
	//	std::vector<sf::Vertex> vertices = {
	//		{ { -0.5f, -0.5f,  0.0f }, { 1.0f, 0.0f, 0.0f } },
	//		{ { -0.5f,  0.5f,  0.0f }, { 0.0f, 1.0f, 0.0f } },
	//		{ {  0.5f,  0.5f,  0.0f }, { 0.0f, 0.0f, 1.0f } }
	//	};
	//	uint32_t verticesSize = (uint32_t)(vertices.size() * sizeof(vertices[0]));

	//	// Setup indices
	//	std::vector<uint32_t> indices = { 0, 1, 2 };
	//	uint32_t indicesSize = (uint32_t)(indices.size() * sizeof(indices[0]));

	//	VkMemoryAllocateInfo memAlloc = {};
	//	memAlloc.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
	//	VkMemoryRequirements memReqs;
	//	void* data;

	//	struct StagingBuffer {
	//		VkDeviceMemory memory;
	//		VkBuffer buffer;
	//	};

	//	struct {
	//		StagingBuffer vertices;
	//		StagingBuffer indices;
	//	} stagingBuffers;

	//	// Allocate command buffer for copy operation
	//	VkCommandBufferAllocateInfo cmdBufInfo = {};
	//	cmdBufInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	//	cmdBufInfo.commandPool = vkContext.commandPool;
	//	cmdBufInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	//	cmdBufInfo.commandBufferCount = 1;

	//	VkCommandBuffer copyCommandBuffer;
	//	vkAllocateCommandBuffers(vkContext.device, &cmdBufInfo, &copyCommandBuffer);

	//	// First copy vertices to host accessible vertex buffer memory
	//	VkBufferCreateInfo vertexBufferInfo = {};
	//	vertexBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	//	vertexBufferInfo.size = verticesSize;
	//	vertexBufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

	//	vkCreateBuffer(vkContext.device, &vertexBufferInfo, nullptr, &stagingBuffers.vertices.buffer);

	//	vkGetBufferMemoryRequirements(vkContext.device, stagingBuffers.vertices.buffer, &memReqs);
	//	memAlloc.allocationSize = memReqs.size;
	//	getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, &memAlloc.memoryTypeIndex);
	//	vkAllocateMemory(vkContext.device, &memAlloc, nullptr, &stagingBuffers.vertices.memory);

	//	vkMapMemory(vkContext.device, stagingBuffers.vertices.memory, 0, verticesSize, 0, &data);
	//	memcpy(data, vertices.data(), verticesSize);
	//	vkUnmapMemory(vkContext.device, stagingBuffers.vertices.memory);
	//	vkBindBufferMemory(vkContext.device, stagingBuffers.vertices.buffer, stagingBuffers.vertices.memory, 0);

	//	// Then allocate a gpu only buffer for vertices
	//	vertexBufferInfo.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
	//	vkCreateBuffer(vkContext.device, &vertexBufferInfo, nullptr, &vertexBuffer);
	//	vkGetBufferMemoryRequirements(vkContext.device, vertexBuffer, &memReqs);
	//	memAlloc.allocationSize = memReqs.size;
	//	getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &memAlloc.memoryTypeIndex);
	//	vkAllocateMemory(vkContext.device, &memAlloc, nullptr, &vertexBufferMemory);
	//	vkBindBufferMemory(vkContext.device, vertexBuffer, vertexBufferMemory, 0);

	//	// Next copy indices to host accessible index buffer memory
	//	VkBufferCreateInfo indexBufferInfo = {};
	//	indexBufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	//	indexBufferInfo.size = indicesSize;
	//	indexBufferInfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

	//	vkCreateBuffer(vkContext.device, &indexBufferInfo, nullptr, &stagingBuffers.indices.buffer);
	//	vkGetBufferMemoryRequirements(vkContext.device, stagingBuffers.indices.buffer, &memReqs);
	//	memAlloc.allocationSize = memReqs.size;
	//	getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT, &memAlloc.memoryTypeIndex);
	//	vkAllocateMemory(vkContext.device, &memAlloc, nullptr, &stagingBuffers.indices.memory);
	//	vkMapMemory(vkContext.device, stagingBuffers.indices.memory, 0, indicesSize, 0, &data);
	//	memcpy(data, indices.data(), indicesSize);
	//	vkUnmapMemory(vkContext.device, stagingBuffers.indices.memory);
	//	vkBindBufferMemory(vkContext.device, stagingBuffers.indices.buffer, stagingBuffers.indices.memory, 0);

	//	// And allocate another gpu only buffer for indices
	//	indexBufferInfo.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
	//	vkCreateBuffer(vkContext.device, &indexBufferInfo, nullptr, &indexBuffer);
	//	vkGetBufferMemoryRequirements(vkContext.device, indexBuffer, &memReqs);
	//	memAlloc.allocationSize = memReqs.size;
	//	getMemoryType(memReqs.memoryTypeBits, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT, &memAlloc.memoryTypeIndex);
	//	vkAllocateMemory(vkContext.device, &memAlloc, nullptr, &indexBufferMemory);
	//	vkBindBufferMemory(vkContext.device, indexBuffer, indexBufferMemory, 0);

	//	// Now copy data from host visible buffer to gpu only buffer
	//	VkCommandBufferBeginInfo bufferBeginInfo = {};
	//	bufferBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
	//	bufferBeginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

	//	vkBeginCommandBuffer(copyCommandBuffer, &bufferBeginInfo);

	//	VkBufferCopy copyRegion = {};
	//	copyRegion.size = verticesSize;
	//	vkCmdCopyBuffer(copyCommandBuffer, stagingBuffers.vertices.buffer, vertexBuffer, 1, &copyRegion);
	//	copyRegion.size = indicesSize;
	//	vkCmdCopyBuffer(copyCommandBuffer, stagingBuffers.indices.buffer, indexBuffer, 1, &copyRegion);

	//	vkEndCommandBuffer(copyCommandBuffer);

	//	// Submit to queue
	//	VkSubmitInfo submitInfo = {};
	//	submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
	//	submitInfo.commandBufferCount = 1;
	//	submitInfo.pCommandBuffers = &copyCommandBuffer;

	//	vkQueueSubmit(vkContext.graphicsQueue, 1, &submitInfo, VK_NULL_HANDLE);
	//	vkQueueWaitIdle(vkContext.graphicsQueue);

	//	vkFreeCommandBuffers(vkContext.device, vkContext.commandPool, 1, &copyCommandBuffer);

	//	vkDestroyBuffer(vkContext.device, stagingBuffers.vertices.buffer, nullptr);
	//	vkFreeMemory(vkContext.device, stagingBuffers.vertices.memory, nullptr);
	//	vkDestroyBuffer(vkContext.device, stagingBuffers.indices.buffer, nullptr);
	//	vkFreeMemory(vkContext.device, stagingBuffers.indices.memory, nullptr);

	//	std::cout << "set up vertex and index buffers" << std::endl;

	//	// Binding and attribute descriptions
	//	vertexBindingDescription.binding = 0;
	//	vertexBindingDescription.stride = sizeof(vertices[0]);
	//	vertexBindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

	//	// vec2 position
	//	vertexAttributeDescriptions.resize(2);
	//	vertexAttributeDescriptions[0].binding = 0;
	//	vertexAttributeDescriptions[0].location = 0;
	//	vertexAttributeDescriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;

	//	// vec3 color
	//	vertexAttributeDescriptions[1].binding = 0;
	//	vertexAttributeDescriptions[1].location = 1;
	//	vertexAttributeDescriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
	//	vertexAttributeDescriptions[1].offset = sizeof(float) * 3;
	//}
};
