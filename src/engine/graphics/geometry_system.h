#pragma once

#include "../componentsystem.h"
#include "./geometry_component.h"

class GeometryComponentSystem : public ComponentSystem<GeometryComponent> {

	void Update(SystemIterable<GeometryComponent>& state) {

		for (auto& obj : state) {

			auto& [geometry] = obj;

			std::cout << geometry->loadedToGpu;
		}
		std::cout << '\n';
	}
};