#pragma once

#include "../componentsystem.h"

struct GeometryComponent : public Component {
	std::vector<GLfloat> uv;
	std::vector<GLfloat> vertices;

	bool loadedToGpu = false;
};
