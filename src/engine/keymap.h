#pragma once

#include <SFML/Window/Keyboard.hpp>

#include <array>

namespace Tests {
	struct Test_Keymap;
}

// Workflow:
// - swapState
// - Update all keys
// - use keyDown, keyPressed, etc

class KeyMap {
	friend class SubsystemWindow;
	friend struct Tests::Test_Keymap;

	// bool == key is down
	std::array<bool, sf::Keyboard::KeyCount> previousState = { false };
	std::array<bool, sf::Keyboard::KeyCount> currentState = { false };

	void updateKey(sf::Keyboard::Key key, bool isPressed) {
		currentState[key] = isPressed;
	}

	void swapState() {
		previousState = currentState;
	}

public:
	KeyMap() {}

	bool keyDown(sf::Keyboard::Key key) {
		return currentState[key];
	}
	bool keyPressed(sf::Keyboard::Key key) {
		return currentState[key] && !previousState[key];
	}
	bool keyReleased(sf::Keyboard::Key key) {
		return !currentState[key] && previousState[key];
	}
};