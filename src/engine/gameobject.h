#pragma once

#include "globalcontext.h"

class GameObject {
public:
	virtual void Update(GlobalContext& context) = 0;
};
