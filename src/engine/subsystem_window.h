#pragma once

#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#include "subsystem.h"
#include "globalcontext.h"

class SubsystemWindow : public Subsystem {
public:
	void init(GlobalContext& context) {

		// Create the main window

		context.window = std::make_unique<sf::Window>(sf::VideoMode(800, 600), "Aelia Engine", sf::Style::Default);
		context.window->setActive(false); // disable OpenGL context
	}

	void loop(GlobalContext& context) {

		// Process events
		sf::Event event;
		while (context.window->pollEvent(event))
		{
			if (event.type == sf::Event::KeyPressed) {
				context.keyMap.updateKey(event.key.code, true);
			}
			if (event.type == sf::Event::KeyReleased) {
				context.keyMap.updateKey(event.key.code, false);
			}

			// Close window: exit
			if (event.type == sf::Event::Closed)
			{
				context.isApplicationRunning = false;
				context.window->close();
			}

			if (event.type == sf::Event::KeyPressed) {


				// Escape key: exit
				if (event.key.code == sf::Keyboard::Escape)
				{
					context.isApplicationRunning = false;
					context.window->close();
				}

				// Adjust the viewport when the window is resized
				if (event.type == sf::Event::Resized)
				{
					context.windowResized = true;
					context.windowHeight = event.size.height;
					context.windowWidth = event.size.width;

					// Make the window the active window for OpenGL calls
					//context.window->setActive(true);

					//glViewport(0, 0, event.size.width, event.size.height);
					//projMatrix = glm::ortho(
					//	-(float)event.size.width/2, 
					//	(float)event.size.width/2, 
					//	-(float)event.size.height/2, 
					//	(float)event.size.height/2, 
					//	-500.f, 500.f);
					//context.mainCamera.setProjectionPerspective(90.f, (float)event.size.width, (float)event.size.height, 1.0f, 500.0f);
				}
			}
		}

		context.keyMap.swapState();
	}
	void end(GlobalContext& context) {}
};