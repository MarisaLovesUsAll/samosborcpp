////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////

#include <GL/glew.h>

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

#include <filesystem>
#include <iostream>

#include "./engine/camera.h"
#include "./engine/keymap.h"
#include "./engine/globalcontext.h"
#include "./game/cameraobject.h"

#include "./engine/componentsystem.h"

#include "engine/subsystem.h"
#include "engine/graphics/subsystem_graphics.h"
#include "engine/graphics/geometry_component.h"
#include "engine/subsystem_window.h"

#include "game/cube_data.h"

#ifndef GL_SRGB8_ALPHA8
#define GL_SRGB8_ALPHA8 0x8C43
#endif

std::string resourcesDir()
{
#ifdef SFML_SYSTEM_IOS
	return "";
#else
	return "resources\\";
#endif
}

/*
2 ������ ��������� - ������� (���������� �� �������� ��� � �� ������)
- ����������

https://www.khronos.org/opengl/wiki/Transform_Feedback

*/



void FuckOpengl() {

	GLenum errorCode = 0;
	while (errorCode = glGetError()) {
		std::cout << "OpenGL Error " << errorCode << '\n';
	}
}


std::string vertex_shader_source = R"(

#version 420

layout (location = 0) in vec3 vertex_position;
layout (location = 1) in vec2 uv;
uniform mat4 mvp_matrix;

out vec2 fragUV;

void main() {
  gl_Position = mvp_matrix * vec4(vertex_position, 1.0);
  fragUV = uv;
}
)";

std::string frag_shader_source = R"(

#version 420

in vec2 fragUV;
uniform sampler2D textureSampler;
out vec4 out_Color;

void main() {
  out_Color = vec4(texture( textureSampler, fragUV ).rgb, 1.0f);
}
)";

class MainApplication {


	std::unique_ptr<SubsystemGraphics> systemGraphics;
	std::unique_ptr<SubsystemWindow> systemWindow;

	// not the brightest of an idea but whatever
	GlobalContext context;

	ECSManager<TestSystem, TestComponent, TestComponent2> ecsManager;

public:

	MainApplication() {
		systemWindow = std::make_unique<SubsystemWindow>();
		systemGraphics = std::make_unique<SubsystemGraphics>();
	}
	
	void init() {
		std::cout << "current path: " << std::filesystem::current_path() << std::endl;

		systemWindow->init(context);
		systemGraphics->init(context);

		// TODO: scene loader

		Entity entity = ecsManager.CreateGameObject<GeometryComponent>();
		GeometryComponent* component = ecsManager.GetComponent<GeometryComponent>(entity);

		component->vertices = std::vector<GLfloat>(std::begin(cube), std::end(cube));
		component->uv = std::vector<GLfloat>(std::begin(cube_uv), std::end(cube_uv));
	}

	void loop() {
		while (context.isApplicationRunning) {
			try {
				systemWindow->loop(context);

				ecsManager.UpdateAllSystems();

				systemGraphics->loop(context);
			}
			catch (...) {
				context.isApplicationRunning = false;
			}
		}
	}

	void end() {
		systemWindow->end(context);
		systemGraphics->end(context);
	}
};

int main()
{
	MainApplication mainApplication;
	mainApplication.init();
	mainApplication.loop();
	mainApplication.end();

	return EXIT_SUCCESS;
}